# Specyfikacja

Urządzenie jest uniwersalną płytką uruchomieniową do MCU STM32G051K8T6 (https://www.st.com/en/microcontrollers-microprocessors/stm32g051k8.html).

# Projekt PCB

Schemat: [doc/STM32G051K8_EVB_V1_0_SCH.pdf](doc/STM32G051K8_EVB_V1_0_SCH.pdf)

Widok 3D: [doc/STM32G051K8_EVB_V1_0_3D.pdf](doc/STM32G051K8_EVB_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

# Licencja

MIT
